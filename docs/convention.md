# Convention de code

## Structure des commits

Les commits devraient être le plus atomiques possible.

Structure du message d'un commit :

```shell
<type>([optional scope]): <description>

[optional body]

[Issue number]
````

Exemple:

```shell
feat(n° Ticket): short commit message

Commit description [optional]

```

## Merge avec la branche master

Les commits de merge sont prohibés. Il faut plutôt utiliser `rebase`.

```shell
git checkout branch
git pull origin -i develop
git rebase -i origin develop -- autosquash
git push origin +master
```
