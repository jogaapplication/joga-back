from apps.entity.validator import FinancialInstitutionValidator
from marshmallow import ValidationError
from apps.entity import repository as financial_institution_repository
from apps.utils.exceptions import FinancialInstitutionException, PayloadException, \
    InternalException, EmployeeException
from apps.entity.models import FinancialInstitution
from django.contrib.auth.models import User

financial_institution_validator = FinancialInstitutionValidator()


def get_financial_institution_by_identifier(id):
    try:
        return financial_institution_repository.retrieve_financial_institution_by_id(id)
    except FinancialInstitution.DoesNotExist:
        raise FinancialInstitutionException('The financial institution is not found')

def get_all_financial_institutions():
    return financial_institution_repository.get_all_financial_institutions()

def get_all_financial_institution_by_country(country):
    try:
        return financial_institution_repository.retrieve_financial_institution_by_country(country)
    except FinancialInstitution.DoesNotExist:
        raise FinancialInstitutionException('The financial institution is not found')

def _validate_financial_institution_creation_payload(payload):
    try:
        financial_institution_validator.load(payload)
    except ValidationError as e:
        raise PayloadException(str(e))


def employee_login(payload):

    username = payload.get('username')
    password = payload.get('password')
    try:
        login_result = financial_institution_repository.check_employee_password(
            username, password)
    except User.DoesNotExist:
        raise EmployeeException('User cannot be found')
    except Exception as e:
        raise EmployeeException(str(e))
    if login_result:
        financial_institution_employee = financial_institution_repository.get_financial_institution_employee(username)
        return financial_institution_employee.json
    raise EmployeeException('No connection possible')
