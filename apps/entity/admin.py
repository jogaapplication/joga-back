from django.contrib import admin
from apps.entity.models import FinancialInstitution, FinancialInstitutionType, Employee, FinancialInstitutionEmployee

class FinancialInstitutionAdmin(admin.ModelAdmin):

    list_display = ('id', 'short_name', 'full_name', 'country', 'code')
    search_fields = ['short_name', 'full_name', 'country']


admin.site.register(FinancialInstitution, FinancialInstitutionAdmin)

class FinancialInstitutionTypeAdmin(admin.ModelAdmin):

    list_display = ('id', 'short_name', 'full_name', 'description')
    search_fields = ['short_name', 'full_name']


admin.site.register(FinancialInstitutionType, FinancialInstitutionTypeAdmin)


class EmployeeAdmin(admin.ModelAdmin):

    list_display = ('id', 'user', 'financial_institution')


admin.site.register(Employee, EmployeeAdmin)

class FinancialInstitutionEmployeeAdmin(admin.ModelAdmin):

    list_display = ('id', 'user', 'financial_institution')

admin.site.register(FinancialInstitutionEmployee, FinancialInstitutionEmployeeAdmin)
