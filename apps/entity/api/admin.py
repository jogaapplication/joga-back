from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from django.conf.urls import url
from tastypie.utils import trailing_slash
from apps.entity import controller as financial_institution_controller
from apps.utils.exceptions import FinancialInstitutionException
from tastypie.http import HttpUnauthorized, HttpForbidden,\
    HttpCreated, HttpApplicationError, HttpConflict, HttpNoContent
from tastypie.authorization import Authorization
from marshmallow import ValidationError

API_FORMAT = 'application/json'

class FinancialInstitutionEmployeeResource(ModelResource):
    class Meta:
        queryset = financial_institution_controller.get_all_financial_institutions()
        list_allowed_methods = ['get', 'post']
        detail_allowed_methods = ['get', 'post', 'put', 'delete']
        resource_name = 'corporate'
        filtering = {
            'slug': ALL,
            'identifier': ALL,
            'created': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
        }
        authorization = Authorization()

    def determine_format(self, request):
        return API_FORMAT

    def prepend_urls(self):
        return [

            url(rf'^%s/login%s$' % (self._meta.resource_name,
                                    trailing_slash()), self.wrap_view('login')),
            url(rf'^%s/customers$' % (self._meta.resource_name), self.wrap_view('bank_customers')),
        ]

    def bank_customers(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        return self.create_response(request, {}, HttpNoContent)

    def login(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        payload = self.deserialize(request, request.body)
        try:
            response = financial_institution_controller.employee_login(payload)
        except Exception as e:
            return self.create_response(request, {'error': str(e)}, HttpUnauthorized)
        return self.create_response(request, response)


class FinancialInstitutionResource(ModelResource):

    class Meta:
        queryset = financial_institution_controller.get_all_financial_institutions()
        list_allowed_methods = ['get', 'post']
        detail_allowed_methods = ['get', 'post', 'put', 'delete']
        resource_name = 'financial_institution'
        filtering = {
            'slug': ALL,
            'id': ALL_WITH_RELATIONS,
            'created': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
        }
        authorization = Authorization()

    def prepend_urls(self):
        return [

            url(r"^(?P<resource_name>%s)/(?P<id>[\w\d_.-]+)/customers$" % self._meta.resource_name, self.wrap_view('bank_customers'),
                name='api_bank_customers'),
        ]

    def determine_format(self, request):
        return API_FORMAT

    def bank_customers(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        print(request)

        return self.create_response(request, {}, HttpNoContent)
