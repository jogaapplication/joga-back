from apps.entity.api import FinancialInstitutionEmployeeResource, FinancialInstitutionResource

resources = []
resources.append(FinancialInstitutionEmployeeResource())
resources.append(FinancialInstitutionResource())
