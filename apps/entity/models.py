from django.db import models
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django_countries.fields import CountryField


class FinancialInstitutionType(models.Model):
    short_name = models.CharField(max_length=200)
    full_name = models.CharField(max_length=200, blank=True, null=True)
    description = models.CharField(max_length=2000, blank=True, null=True)

    def __str__(self):
        return self.short_name

    class Meta:
        verbose_name = _('Financial Institution type')
        verbose_name_plural = _('Financial Institutions type')
        app_label = 'entity'


class FinancialInstitution(models.Model):
    short_name = models.CharField(max_length=200)
    full_name = models.CharField(max_length=2000, blank=True, null=True)
    alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')
    type_of_financial_institution = models.ForeignKey('FinancialInstitutionType', on_delete=models.SET_NULL, blank=True, null=True)
    code = models.CharField(max_length=11, validators=[alphanumeric], unique=True)
    street_address = models.CharField(max_length=512)
    city = models.CharField(max_length=256)
    postal_code = models.PositiveIntegerField()
    country = CountryField()

    def __str__(self):
        return self.short_name

    @property
    def json(self):
        return {
            'identifier': self.id,
            'short_name': self.short_name,
            'full_name': self.full_name,
            'country': self.country.code
        }

    class Meta:
        verbose_name = _('Financial institution')
        verbose_name_plural = _('Financial institutions')
        app_label = 'entity'

class Employee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    financial_institution = models.ForeignKey('FinancialInstitution', on_delete=models.SET_NULL, blank=True, null=True)
    address = models.CharField(max_length=100, null=True, blank=True)
    status = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'

    @property
    def json(self):

        return {
            'id': self.user.id,
            'username': self.user.username,
            'first_name': self.user.first_name,
            'last_name': self.user.last_name,
            'is_admin': self.user.is_staff,
            'token': 'fake-jwt-token',
            'bank': self.financial_institution.json,

        }


class FinancialInstitutionEmployee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.CharField(max_length=100, null=True, blank=True)
    financial_institution = models.ForeignKey('FinancialInstitution', on_delete=models.SET_NULL, blank=True, null=True)
    address = models.CharField(max_length=100, null=True, blank=True)
    status = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'

    @property
    def json(self):
        return {
            'id': self.user.id,
            'username': self.user.username,
            'first_name': self.user.first_name,
            'email': self.email,
            'last_name': self.user.last_name,
            'is_admin': self.user.is_staff,
            'token': 'fake-jwt-token',
            'bank': self.financial_institution.json,

        }
