from django.apps import AppConfig


class UserInvestmentAccountConfig(AppConfig):
    name = 'user_investment_account'
