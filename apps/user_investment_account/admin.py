from django.contrib import admin
from apps.user_investment_account.models import UserInvestmentAccount

# Register your models here.
class UserInvestmentAccountAdmin(admin.ModelAdmin):

    list_display = ('customer_first_name','customer_last_name','bank_swift_code','iban')
    search_fields = ['bank_swift_code','iban']

    def customer_first_name(self, obj):
        return obj.customer.user.first_name

    def customer_last_name(self, obj):
        return obj.customer.user.last_name

admin.site.register(UserInvestmentAccount, UserInvestmentAccountAdmin)