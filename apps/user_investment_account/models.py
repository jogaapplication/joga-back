from django.db import models
from apps.customer.models import Customer
from django.utils.translation import ugettext_lazy as _

# Create your models here.
class UserInvestmentAccount(models.Model):

    customer = models.ForeignKey(Customer,on_delete=models.CASCADE)
    # Utiliser une librairie de gestion de l'iban et du swift
    bank_swift_code = models.CharField(max_length=10)
    iban = models.CharField(max_length=30)

    # Dates
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.customer.user.first_name} {self.iban}'

    class Meta:
        verbose_name = _('UserInvestmentAccount')
        verbose_name_plural = _('UserInvestmentAccounts')
        app_label = 'user_investment_account'