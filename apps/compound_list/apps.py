from django.apps import AppConfig


class CompoundListConfig(AppConfig):
    name = 'compound_list'
