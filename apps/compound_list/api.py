from tastypie.resources import ModelResource, ALL
from django.conf.urls import url
from tastypie.utils import trailing_slash
from apps.compound_list import controller as compound_list_controller
from apps.utils.exceptions import CompoundListException
from tastypie.http import HttpUnauthorized, HttpForbidden,\
    HttpCreated, HttpApplicationError, HttpConflict
from tastypie.authorization import Authorization
from marshmallow import ValidationError
from apps.utils.api import MultiPartResource

API_FORMAT = 'application/json'

class CompoundListResource(ModelResource):

    class Meta:
        queryset = compound_list_controller.get_all_compound_lists()
        list_allowed_methods = ['get']
        detail_allowed_methods = ['get']
        resource_name = 'compound_list'
        filtering = {
            'slug': ALL,
            'created': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
        }
        authorization = Authorization()

    def determine_format(self, request):
        return API_FORMAT

    def prepend_urls(self):
        return [
            url(rf'^%s/create_new$' %
                self._meta.resource_name, self.wrap_view('create_new')),
        ]

    def create_new(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        payload = self.deserialize(request, request.body)
        try:
            response = compound_list_controller.new_compound_list(payload)
        except CompoundListException as e:
            return self.create_response(request, {'error': str(e)}, HttpConflict)
        except Exception as e:
            return self.create_response(request, {'error': str(e)}, HttpApplicationError)
        except ValidationError as e:
            return self.create_response(request, {'error': str(e)}, HttpApplicationError)

        return self.create_response(request, response, HttpCreated)