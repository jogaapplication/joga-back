from apps.compound_list.models import CompoundList

def retrieve_compound_list_by_identifier(id: str):
    return Bank.objects.get(id=id)

def create_compound_list(short_name, full_name, description) -> CompoundList:
    compound_list = CompoundList()
    compound_list.short_name = short_name
    compound_list.full_name = full_name
    compound_list.description = description

    compound_list.save()
    return compound_list

def get_all_compound_lists():
    return CompoundList.objects.all()