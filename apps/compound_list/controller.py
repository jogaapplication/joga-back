from apps.compound_list.validator import CompoundListValidator
from marshmallow import ValidationError
from apps.compound_list import repository as compound_list_repository
from apps.utils.exceptions import PayloadException, \
    InternalException, CompoundListException
from apps.compound_list.models import CompoundList

compound_list_validator = CompoundListValidator()

def new_compound_list(payload):
    _validate_compound_list_creation_payload(payload)
    short_name = payload.get('short_name')
    full_name = payload.get('full_name')
    description = payload.get('description')

    compound_list = compound_list_repository.create_compound_list(short_name,full_name,description)

    return {'id': compound_list.id}

def get_compound_list_by_identifier(identifier):
    try:
        return compound_list_repository.retrieve_compound_list_by_identifier(identifier)
    except CompoundList.DoesNotExist:
        raise CompoundListException('Compound list not found')

def get_all_compound_lists():
    return compound_list_repository.get_all_compound_lists()


def _validate_compound_list_creation_payload(payload):
    try:
        compound_list_validator.load(payload)
    except ValidationError as e:
        raise PayloadException(str(e))