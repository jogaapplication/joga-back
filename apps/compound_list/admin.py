from django.contrib import admin

# Register your models here.
from apps.compound_list.models import CompoundList

# Register your models here.
class CompoundListAdmin(admin.ModelAdmin):

    list_display = ('id','short_name', 'full_name','description')
    search_fields = ['short_name', 'full_name']

admin.site.register(CompoundList, CompoundListAdmin)