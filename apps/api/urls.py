from django.urls import path
from django.conf.urls import include
from tastypie.api import Api

from apps.customer.urls import resources as customer_api_urls
from apps.entity.urls import resources as financial_institution_api_urls
from apps.financial_product.urls import resources as financial_product_api_urls
from apps.subscription.urls import resources as subscription_api_urls


v1_api = Api(api_name='v1')

for c_url in customer_api_urls:
    v1_api.register(c_url)

for f_url in financial_institution_api_urls:
    v1_api.register(f_url)


for financial_product_url in financial_product_api_urls:
    v1_api.register(financial_product_url)

for subscription_url in subscription_api_urls:
    v1_api.register(subscription_url)

urlpatterns = [
    path('', include(v1_api.urls)),
]
