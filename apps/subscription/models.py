from django.db import models
from apps.financial_product.models import FinancialProduct
from apps.customer.models import Customer
from django.utils.translation import ugettext_lazy as _
from djmoney.models.fields import MoneyField
from .constants import DEFAULT_CURRENCY
from enum import Enum
from decimal import Decimal
class FrequencyValues(Enum):
    MONTHLY = 'MONTHLY'
    WEEKLY = 'WEEKLY'

class SubscriptionStatus(Enum):
    PENDING = 'PENDING'
    FAILED = 'FAILED'


# Create your models here.
class Subscription(models.Model):
    # Product
    financial_product = models.ForeignKey(FinancialProduct, on_delete=models.SET_NULL, null=True)

    # User information
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True)
    payment_method = ""

    # Status and scheduling
    active = models.BooleanField(default=True)
    scheduled = models.BooleanField(default=False)
    status = models.CharField(max_length=20, choices=[
        (tag.value, tag.value) for tag in SubscriptionStatus], default=SubscriptionStatus.PENDING.value)

    # Amount information
    amount = MoneyField(max_digits=14, decimal_places=2, default_currency=DEFAULT_CURRENCY, blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    frequency = models.CharField(max_length=50, choices=[
                                (tag.value, tag.value) for tag in FrequencyValues], default='MONTHLY')

    def __str__(self):
        return f'{self.id}'


    class Meta:
        verbose_name = _('Subscription')
        verbose_name_plural = _('Subscriptions')
        app_label = 'subscription'
