from apps.subscription.validator import SubscriptionValidator
from marshmallow import ValidationError
from apps.subscription import repository as subscription_repository
from apps.utils.exceptions import SubscriptionException, PayloadException, \
    InternalException
from apps.subscription.models import Subscription

subscription_validator = SubscriptionValidator()

def new_subscription(payload):
    _validate_subscription_creation_payload(payload)
    # Status and scheduling
    active = models.boolean(default=True)
    subscription = subscription_repository.create_subscription(name,type,initial_amount)
    return {'identifier': subscription.id}

def get_all_subscriptions():
    return subscription_repository.get_all_subscriptions()

def get_all_subscriptions_by_country(country):
    try:
        return subscription_repository.retrieve_subscription_by_country(country)
    except Subscription.DoesNotExist:
        raise SubscriptionException('Subscription not found')

def get_all_subscriptions_by_financial_institution(financial_institution):
    try:
        return subscription_repository.retrieve_subscription_by_financial_institution(financial_institution)
    except Subscription.DoesNotExist:
        raise SubscriptionException('Subscription not found')

def _validate_subscription_creation_payload(payload):
    try:
        subscription_validator.load(payload)
    except ValidationError as e:
        raise PayloadException(str(e))