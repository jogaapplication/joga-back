from apps.subscription.models import Subscription


def create_subscription(short_name, full_name, description) -> Subscription:
    subscription = Subscription()
    subscription.short_name = short_name
    subscription.full_name = full_name
    subscription.description = description
    subscription.save()
    return subscription

def get_all_subscriptions():
    return Subscription.objects.all()