from decimal import Decimal
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from django.conf.urls import url
from tastypie.utils import trailing_slash
from apps.subscription import controller as subscription_controller
from apps.utils.exceptions import InvestmentTypeException
from tastypie.http import HttpUnauthorized, HttpForbidden,\
    HttpCreated, HttpApplicationError, HttpConflict
from tastypie.authorization import Authorization
from marshmallow import ValidationError
from apps.utils.api import MultiPartResource
from apps.financial_product.api import FinancialProductResource
from apps.customer.api.me import CustomerResource
from tastypie.fields import ForeignKey, CharField

API_FORMAT = 'application/json'

class SubscriptionResource(ModelResource):
    financial_product = ForeignKey(FinancialProductResource, 'financial_product', full=True)
    customer = ForeignKey(CustomerResource, 'customer', full=True)
    amount = CharField(attribute='amount')

    class Meta:
        queryset = subscription_controller.get_all_subscriptions()
        list_allowed_methods = ['get', 'post']
        resource_name = 'subscriptions'
        filtering = {
            'slug': ALL,
            'identifier': ALL,
            'status': ALL,
            'created': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'financial_product': ALL_WITH_RELATIONS
        }

    def determine_format(self, request):
        return API_FORMAT

    def alter_list_data_to_serialize(self, request, data_dict):
        del data_dict['meta']
        return data_dict

    