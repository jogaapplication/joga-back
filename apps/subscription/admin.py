from django.contrib import admin
from apps.subscription.models import Subscription

class SubscriptionAdmin(admin.ModelAdmin):

    list_display = ('id','customer', 'active', 'scheduled', "financial_product", 'created', 'updated')
    search_fields = ['id','customer', 'active', 'scheduled', "financial_product" , 'created', 'updated']
    save_as = True

admin.site.register(Subscription, SubscriptionAdmin)