class CustomerException(Exception):
    pass


class PayloadException(Exception):
    pass

class InternalException(Exception):
    pass

class FinancialInstitutionException(Exception):
    pass

class InvestmentTypeException(Exception):
    pass

class CompoundListException(Exception):
    pass

class FinancialProductException(Exception):
    pass

class FinancialInstitutionTypeException(Exception):
    pass

class SubscriptionException(Exception):
    pass

class ProofStorageException(Exception):
    pass

class EmployeeException(Exception):
    pass