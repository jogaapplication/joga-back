# Generated by Django 3.1.6 on 2022-02-07 19:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0017_auto_20220201_2256'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='identifier',
            field=models.CharField(blank=True, default='4498689594', max_length=50, null=True),
        ),
    ]
