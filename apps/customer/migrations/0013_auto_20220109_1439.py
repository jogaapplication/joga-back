# Generated by Django 3.1.6 on 2022-01-09 13:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0012_auto_20220109_1438'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='identifier',
            field=models.CharField(blank=True, default='9702028705', max_length=50, null=True),
        ),
    ]
