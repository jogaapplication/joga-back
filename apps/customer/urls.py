from apps.customer.api.auth import AuthResource
from apps.customer.api.me import CustomerResource, ProofTypeResource

resources = []

resources.append(AuthResource())
resources.append(CustomerResource())
resources.append(ProofTypeResource())
