import logging
from apps.customer.validator import CustomerValidator, CustomerLoginValidator, CustomerUpdatePinValidator
from marshmallow import ValidationError
from apps.customer import repository as customer_repository
from django.contrib.auth.models import User
from apps.utils.exceptions import CustomerException, PayloadException, InternalException
from apps.customer.models import Customer
from apps.utils.tools import get_totp, send_sms, is_valid_otp

customer_validator = CustomerValidator()
customer_login_validator = CustomerLoginValidator()
customer_update_pin_validator = CustomerUpdatePinValidator()

FORMAT = '%(levelname)s %(asctime)-15s %(name)s %(message)s'
logging.basicConfig(format=FORMAT)
logger = logging.getLogger(__name__)

def get_users():
    return User.objects.all()

def new_customer(payload):

    full_name = payload.get('full_name')
    phone_number = payload.get('phone_number')
    email = payload.get('email')
    password = payload.get('password')

    try:
        customer_repository.retreive_customer_by_email(email)
        raise CustomerException('This user already exists')
    except User.DoesNotExist:
        pass

    names = full_name.split(' ')
    created_user = customer_repository.create_user(
        names, phone_number, email, password)

    customer = customer_repository.create_customer(created_user)
    return customer.json


def get_all_customers():
    return customer_repository.retreive_all_customers()


def get_all_proof_types():
    return customer_repository.retreive_all_proof_types()


def get_all_proofs():
    return customer_repository.retreive_all_proofs()


def _validate_customer_creation_payload(payload):
    try:
        customer_validator.load(payload)
    except ValidationError as e:
        raise PayloadException(str(e))


def customer_login(payload):
    try:
        customer_login_validator.validate(payload)
    except ValidationError as e:
        raise PayloadException(str(e))

    phone_number = payload.get('phone_number')
    password = payload.get('password')

    try:
        login_result = customer_repository.check_customer_password(
            phone_number, password)
    except User.DoesNotExist:
        raise CustomerException('User cannot be found')
    except Exception as e:
        raise CustomerException(str(e))

    if login_result:
        customer = customer_repository.get_customer(phone_number)
        return customer.json
    raise CustomerException('No connection possible')


def add_proof_to_customer_account(identifier, proof_type, proof_content):
    try:
        customer = customer_repository.retreive_customer_by_identifier(
            identifier)
        customer_repository.create_proof(customer, proof_type, proof_content)
    except Customer.DoesNotExist:
        raise CustomerException('User cannot be found')
    except Exception as e:
        logger.error(e)
        raise InternalException('Enregistrement document impossible')
    return {}


def get_customer_by_identifier(identifier):
    try:
        return customer_repository.retreive_customer_by_identifier(
            identifier)
    except Customer.DoesNotExist:
        raise CustomerException('User cannot be found')


def send_otp(payload: dict):
    try:
        totp = get_totp()
        logger.error(f'Generated TOTP : {totp}')
        send_sms(payload.get('phone_number'), totp)
    except Exception:
        raise InternalException


def check_otp(payload: dict):
    _validate_customer_creation_payload(payload)
    otp = payload.get('otp')
    return is_valid_otp(otp)


def update_pin(payload: dict):
    try:
        customer_update_pin_validator.load(payload)
    except ValidationError as e:
        raise PayloadException(str(e))

    identifier = payload.get('customer_identifier')
    password = payload.get('customer_identifier')
    try:
        customer = customer_repository.retreive_customer_by_identifier(
            identifier)
        customer_repository.set_customer_password(customer, password)
    except Customer.DoesNotExist:
        raise CustomerException('Client introuvable')

    except Exception:
        raise InternalException('Erreur Enregistrement adresse de livraison')
    return {}
