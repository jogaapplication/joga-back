from django.apps import AppConfig


class UserBankAccountConfig(AppConfig):
    name = 'user_bank_account'
