from django.db import models
from apps.customer.models import Customer
from django.utils.translation import ugettext_lazy as _

# Create your models here.
class UserBankAccount(models.Model):

    customer = models.OneToOneField(
        Customer,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    #Utiliser une librairie de gestion de l'iban et du swift
    bank_swift_code = models.CharField(max_length=10)
    iban = models.CharField(max_length=30)

    # Dates
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.customer.user.first_name} {self.iban}'

    class Meta:
        verbose_name = _('UserBankAccount')
        verbose_name_plural = _('UserBankAccounts')
        app_label = 'user_bank_account'