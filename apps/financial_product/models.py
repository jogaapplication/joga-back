from django.db import models
from djmoney.money import Currency, Money
from decimal import Decimal
from apps.entity.models import FinancialInstitution
from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField
from apps.customer.models import ProofType
from djmoney.models.fields import MoneyField
from .constants import DEFAULT_CURRENCY, CURRENCY_CHOICES
from django.core.validators import (
    MinValueValidator,
    MaxValueValidator,
)
from apps.customer import controller as customer_controller
# from apps.compound_list import controller as compound_list_controller
from multiselectfield import MultiSelectField


class CompoundList(models.Model):
    short_name = models.CharField(max_length=200)
    full_name = models.CharField(max_length=200, blank=True, null=True)
    description = models.CharField(max_length=2000, blank=True, null=True)

    def __str__(self):
        return f'{self.short_name}'

    class Meta:
        verbose_name = _('compounding')
        verbose_name_plural = _('compoundings')

class InvestmentType(models.Model):
    short_name = models.CharField(max_length=200)
    full_name = models.CharField(max_length=200, blank=True, null=True)
    description = models.CharField(max_length=2000, blank=True, null=True)

    def __str__(self):
        return self.short_name

    class Meta:
        verbose_name = _('Investment type')
        verbose_name_plural = _('Investments type')
        app_label = 'financial_product'

# Create your models here.
class FinancialProduct(models.Model):

    name = models.CharField(max_length=200)
    description = models.TextField(max_length=2000, blank=True, null=True)
    investment_type = models.ForeignKey('InvestmentType', on_delete=models.SET_NULL, blank=True, null=True)

    financial_institution = models.ForeignKey(FinancialInstitution, on_delete=models.CASCADE)

    initial_amount = MoneyField(max_digits=14,
                                decimal_places=2,
                                default=Money(0, DEFAULT_CURRENCY),
                                default_currency=DEFAULT_CURRENCY,
                                currency_choices=CURRENCY_CHOICES )

    minimum_duration = models.DurationField(help_text='in years')
    maximum_duration = models.DurationField(null=True, blank=True, help_text='in years')

    gross_nominal_interest_rate = models.DecimalField(default=0,
                                                      validators=[MinValueValidator(0), MaxValueValidator(100)],
                                                      decimal_places=2,
                                                      max_digits=5,
                                                      help_text='Interest rate from 0 - 100 in (%)'
                                                      )

    minimum_amount = MoneyField(max_digits=14,
                                decimal_places=2,
                                default=Money(0, DEFAULT_CURRENCY),
                                default_currency=DEFAULT_CURRENCY,
                                currency_choices=CURRENCY_CHOICES)
    maximum_amount = MoneyField(max_digits=14,
                                decimal_places=2,
                                default=Money(0, DEFAULT_CURRENCY),
                                default_currency=DEFAULT_CURRENCY,
                                currency_choices=CURRENCY_CHOICES,blank=True, null=True)

    minimum_monthly_amount = MoneyField(max_digits=14,
                                decimal_places=2,
                                default=Money(0, DEFAULT_CURRENCY),
                                default_currency=DEFAULT_CURRENCY,
                                currency_choices=CURRENCY_CHOICES)
    maximum_monthly_amount = MoneyField(max_digits=14,
                                decimal_places=2,
                                default=Money(0, DEFAULT_CURRENCY),
                                default_currency=DEFAULT_CURRENCY,
                                currency_choices=CURRENCY_CHOICES,blank=True, null=True)

    compounding = models.ForeignKey('CompoundList', on_delete=models.CASCADE, blank=True, null=True)

    interest_payment_period = models.ForeignKey('CompoundList', on_delete=models.CASCADE, blank=True, null=True, related_name='interest_payment_period_compounding')
    anticipated_withdrawal = models.ForeignKey('CompoundList', on_delete=models.CASCADE, blank=True, null=True, related_name='anticipated_withdrawal_compounding')
    prolongation_conditions = models.CharField(max_length=200, blank=True, null=True)

    tax_rate = models.DecimalField(
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        decimal_places=2,
        max_digits=5,
        help_text='Country tax rate from 0 - 100',
        null=True,
        default=0
    )
    # TODO: choices=POSSIBLE_COMPOUNDING
    tax_interest_income_period = models.CharField(default="None", max_length=50)

    tax_details = models.CharField(max_length=200, blank=True, null=True)
    customer_conditions = models.CharField(max_length=200, blank=True, null=True)


    detailed_document = models.FileField(upload_to='uploads/', null=True, blank=True)
    detailed_document_url = models.URLField(blank=True, null=True)

    LIST_OF_DOCUMENTS = tuple()
    document_list = MultiSelectField(default='', choices=LIST_OF_DOCUMENTS, null=True, blank=True)

    is_active = models.BooleanField(default=False, editable=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{ self.name } - {self.financial_institution.short_name} - { self.financial_institution.country}'

    class Meta:
        verbose_name = _('Financial product')
        verbose_name_plural = _('Financial products')
        app_label = 'financial_product'

