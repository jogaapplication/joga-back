from apps.financial_product.models import FinancialProduct

def retrieve_financial_product_by_identifier(id: str):
    return FinancialProduct.objects.get(id=id)

def create_financial_product(name,description,country,type,initial_amount,interest,maximum_amount,minimum_monthly_amount,maximum_monthly_amount,minimum_duration,maximum_duration,tax_value,
            tax_details,customer_conditions,financial_institution, is_active) -> FinancialProduct:
    financial_product.name = name,
    financial_product.description = description,
    financial_product.country = country,
    financial_product.type = type,
    financial_product.initial_amount = initial_amount,
    financial_product.interest = interest,
    financial_product.maximum_amount = maximum_amount,
    financial_product.minimum_monthly_amount = minimum_monthly_amount,
    financial_product.maximum_monthly_amount = maximum_monthly_amount,
    financial_product.minimum_duration = minimum_duration,
    financial_product.maximum_duration = maximum_duration,
    financial_product.tax_value = tax_value,
    financial_product.tax_details = tax_details,
    financial_product.customer_conditions = customer_conditions,
    financial_product.financial_institution = financial_institution,
    financial_product.is_active = is_active

    financial_product.save()
    return financial_product

def get_all_financial_products():
    return FinancialProduct.objects.all()

def retrieve_financial_product_by_country(country:str):
    return FinancialProduct.objects.get(country=country)

def retrieve_financial_product_by_financial_institution(financial_institution:str):
    return FinancialProduct.objects.get(financial_institution=financial_institution)

def retrieve_financial_product_by_financial_product_type(type:str):
    return FinancialProduct.objects.get(type=type)