from django.apps import AppConfig


class FinancialProductConfig(AppConfig):
    name = 'financial_product'
