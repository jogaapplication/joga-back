from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from django.conf.urls import url
from tastypie.utils import trailing_slash
from apps.financial_product import controller as financial_product_controller
from apps.utils.exceptions import FinancialProductException
from tastypie.http import HttpUnauthorized, HttpForbidden,\
    HttpCreated, HttpApplicationError, HttpConflict, HttpNoContent
from tastypie.authorization import Authorization
from marshmallow import ValidationError
from apps.utils.api import MultiPartResource
from tastypie.fields import ForeignKey, CharField, DecimalField
from apps.entity.api import FinancialInstitutionResource
API_FORMAT = 'application/json'

class FinancialProductResource(ModelResource):
    financial_institution = ForeignKey(FinancialInstitutionResource, 'financial_institution', full=True)

    initial_amount = CharField(attribute='initial_amount')
    minimum_amount = CharField(attribute='minimum_amount')
    maximum_amount = CharField(attribute='maximum_amount')
    minimum_monthly_amount = CharField(attribute='minimum_monthly_amount')
    maximum_monthly_amount = CharField(attribute='maximum_monthly_amount')

    class Meta:
        queryset = financial_product_controller.get_all_financial_products()
        list_allowed_methods = ['get']
        detail_allowed_methods = ['get', 'post', 'put', 'delete']
        resource_name = 'financial_product'
        filtering = {
            'slug': ALL,
            'id': ALL,
            'created': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'financial_institution': ALL_WITH_RELATIONS,
        }
        authorization = Authorization()

    def determine_format(self, request):
        return API_FORMAT

    def prepend_urls(self):
        return [
            url(r'^create%s$' % trailing_slash(), self.wrap_view(
                'create_new_financial_product'), name='api_create_new_financial_product'),
        ]



    def alter_list_data_to_serialize(self, request, data_dict):
        del data_dict['meta']
        return data_dict

    def create_new_financial_product(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        payload = self.deserialize(request, request.body)
        try:
            response = financial_product_controller.new_financial_product(payload)
        except FinancialProductException as e:
            return self.create_response(
                request, {'error': str(e)}, HttpConflict)
        except Exception as e:
            return self.create_response(
                request, {'error': str(e)}, HttpApplicationError)

        except ValidationError as e:
            return self.create_response(
                request, {'error': str(e)}, HttpApplicationError)
        return self.create_response(request, response, HttpCreated)
