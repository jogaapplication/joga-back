from django.contrib import admin
from apps.financial_product.models import FinancialProduct, InvestmentType, CompoundList


class FinancialProductAdmin(admin.ModelAdmin):

    list_display = ('id', 'name', 'gross_nominal_interest_rate', 'financial_institution')
    search_fields = ['name', 'type']
    save_as = True

admin.site.register(FinancialProduct, FinancialProductAdmin)

class InvestmentTypeAdmin(admin.ModelAdmin):

    list_display = ('id', 'short_name', 'full_name', 'full_name')
    search_fields = ['short_name', 'full_name']
    save_as = True

admin.site.register(InvestmentType, InvestmentTypeAdmin)

class CompListAdmin(admin.ModelAdmin):

    list_display = ('id','short_name', 'full_name','description')
    search_fields = ['short_name', 'full_name']

admin.site.register(CompoundList, CompListAdmin)
