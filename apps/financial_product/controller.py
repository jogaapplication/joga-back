from apps.financial_product.validator import FinancialProductValidator
from marshmallow import ValidationError
from apps.financial_product import repository as financial_product_repository
from apps.utils.exceptions import FinancialProductException, PayloadException, \
    InternalException
from apps.financial_product.models import FinancialProduct

financial_product_validator = FinancialProductValidator()

def new_financial_product(payload):
    _validate_financial_product_creation_payload(payload)
    name = payload.get('name')
    product_type = payload.get('type')
    initial_amount = payload.get('initial_amount')

    try:
        financial_product_repository.retrieve_financial_product_by_identifier(identifier)
        raise FinancialProductException('This financial product already exist')
    except Exception:
        pass

    financial_product = financial_product_repository.create_financial_product(name, product_type, initial_amount)
    return {'identifier': financial_product.id}

def get_financial_product_by_identifier(identifier):
    try:
        return financial_product_repository.retrieve_financial_product_by_identifier(identifier)
    except FinancialProduct.DoesNotExist:
        raise FinancialProductException('Financial Product not found')

def get_all_financial_products():
    return financial_product_repository.get_all_financial_products()

def get_all_financial_products_by_country(country):
    try:
        return financial_product_repository.retrieve_financial_product_by_country(country)
    except FinancialProduct.DoesNotExist:
        raise FinancialProductException('Financial Product not found')

def get_all_financial_products_by_financial_institution(financial_institution):
    try:
        return financial_product_repository.retrieve_financial_product_by_financial_institution(financial_institution)
    except FinancialProduct.DoesNotExist:
        raise FinancialProductException('Financial Product not found')

def get_all_financial_products_by_financial_product_type(type):
    try:
        return financial_product_repository.retrieve_financial_product_by_financial_product_type(type)
    except FinancialProduct.DoesNotExist:
        raise FinancialProductException('FinancialProduct not found')

def update_financial_product(payload):
    # _validate_financial_product_creation_payload(payload)
    try:
        return financial_product_repository.retrieve_financial_product_by_financial_product_type(type)
    except FinancialProduct.DoesNotExist:
        raise FinancialProductException('FinancialProduct not found')

def _validate_financial_product_creation_payload(payload):
    try:
        financial_product_validator.load(payload)
    except ValidationError as e:
        raise PayloadException(str(e))
